A = dlmread("primeiro_dados_14_14_16.csv", "\t");

# se quiser plotar é só 'descomentar' a linha abaixo
# plot(A(1 : end, [1]), A(1 : end, [2]));

i = 2;
counter = 0;

last_time = A(1, [1]);
last_value = A(1, [2]);

while(i <= length(A))

  current_time = A(i, [1]);
  current_value = A(i, [2]);
  
  current_derivative = (last_value - current_value)/(last_time - current_time);
  
  if(i > 2 && last_derivative * current_derivative < 0)
    if(last_derivative < 0)
      counter = counter + 1;
    endif
  endif
  
  last_time = current_time;
  last_value = current_value;
  last_derivative = current_derivative;
  
  i = i + 1;
  
endwhile
  
disp(counter);
